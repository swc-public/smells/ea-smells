var MergeJsonWebpackPlugin = require("merge-jsons-webpack-plugin");
var CopyWebpackPlugin = require("copy-webpack-plugin");
const { TRUE } = require("node-sass");

module.exports = {
  devServer: {
    host: "0.0.0.0",
    port: 8081
  },
  configureWebpack: {
    plugins: [
      new MergeJsonWebpackPlugin({
        debug: true,
        output: {
          groupBy: [
            {
              pattern: "./antipatterns/*.json",
              fileName: "./assets/result.json"
            }
          ]
        },
        globOptions: {
          nosort: false,
          prefixFileName: true
        }
      }),
      new CopyWebpackPlugin([
        {
          from: "./*.md",
          to: "./assets/[name].[ext]"
        },
        {
          from: "./slr/*.md",
          to: "./assets/[name].[ext]"
        },
        {
          from: "./assets/*.json",
          to: "./assets/[name].[ext]"
        }
      ])
    ]
  },
  publicPath: process.env.NODE_ENV === "production" ? "/smells/ea-smells/" : "/"
};
