// configuration variables
export default {
  // path to static asset files (the GitLab pages production path is different)
  assetPath:
    window.location.hostname === "localhost"
      ? "/assets"
      : "https://swc-public.pages.rwth-aachen.de/smells/ea-smells/assets",
};
