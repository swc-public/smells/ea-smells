[![Website](https://img.shields.io/website/https/ba-ea-smells.pages.rwth-aachen.de/ea-smells/.svg.svg?down_message=offline&up_message=online)](https://ba-ea-smells.pages.rwth-aachen.de/ea-smells/)
[![pipeline status](https://git.rwth-aachen.de/ba-ea-smells/ea-smells/badges/master/pipeline.svg)](https://git.rwth-aachen.de/ba-ea-smells/ea-smells/commits/master)
[![Contributions welcome](https://img.shields.io/badge/contributions-welcome-orange.svg)](https://git.rwth-aachen.de/ba-ea-smells/ea-smells/blob/master/CONTRIBUTING.md)
[![License](https://img.shields.io/badge/license-MIT-blue.svg)](https://opensource.org/licenses/MIT)
# Enterprise Architecture Smells

This repository should serve as a knowledge base for Enterprise Architecture Smells.

The first idea of the web-application  was first developed by Bogner et al., and is now adapted to provide information on Enterprise Architecture Smells.

The repository has been created
1. to collect and browse antipatterns in a structured and uniform way (JSON format)
2. to give researchers and practitioners an easy way to access the collection and contribute their antipatterns
3. to version the results

Note that the attribute "sources" currently contains no entries. However, this field already exists, so that in future all sources can be mentioned. On the basis of those references an estimated evidence will be calculated and displayed. Since no sources exist yet, this feature yields no output.

Available under [https://ba-ea-smells.pages.rwth-aachen.de/ea-smells/](https://ba-ea-smells.pages.rwth-aachen.de/ea-smells/)
